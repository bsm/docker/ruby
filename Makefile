BUILDS=$(patsubst %/Dockerfile,build/%,$(wildcard */Dockerfile))
PUSHES=$(patsubst %/Dockerfile,push/%,$(wildcard */Dockerfile))

build: $(BUILDS)
push: $(PUSHES)

.PHONY: build push

# ---------------------------------------------------------------------

DEPS=$(shell grep -h 'FROM' ./*/Dockerfile | grep -v blacksquaremedia | sed -e 's/FROM //')

pull:
	for dep in $(DEPS); do docker pull $$dep; done

build/%: %
	dep=$$(grep -h 'FROM blacksquaremedia/ruby:' $(notdir $@)/Dockerfile | sed -e 's/FROM blacksquaremedia\/ruby\:/build\//'); test -n "$$dep" && $(MAKE) $$dep || true
	docker build -t blacksquaremedia/ruby:$< $</

push/%: % build/%
	docker push blacksquaremedia/ruby:$<

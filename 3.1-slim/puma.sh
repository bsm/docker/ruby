#!/bin/bash
set -eux

cd /app
if [ -x ./config/docker/pre-puma.sh ]; then
  sleep 5
  ./config/docker/pre-puma.sh
fi

bundle exec puma -C /etc/puma.rb

#!/bin/bash
set -eux

cd /app
if [ -x ./config/docker/pre-clockwork.sh ]; then
  sleep 5
  ./config/docker/pre-clockwork.sh
fi

bundle exec clockwork config/clockwork.rb

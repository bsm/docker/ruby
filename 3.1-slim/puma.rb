workers ENV.fetch('PUMA_WORKERS', ENV.fetch('NUM_WORKERS', 0)).to_i

max_threads = ENV.fetch('PUMA_MAX_THREADS', ENV.fetch('NUM_THREADS', 16)).to_i
min_threads = ENV.fetch('PUMA_MIN_THREADS', max_threads).to_i
threads min_threads, max_threads

bind "tcp://0.0.0.0:#{ENV.fetch('PORT', 8080)}"
environment ENV.fetch('RACK_ENV') { 'production' }
preload_app!

plugin :tmp_restart

before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end if defined?(ActiveRecord::Base)
on_worker_boot do
  ActiveRecord::Base.establish_connection
end if defined?(ActiveRecord::Base)

custom_config = '/app/config/puma.rb'
instance_eval(File.read(custom_config)) if File.exist?(custom_config)
